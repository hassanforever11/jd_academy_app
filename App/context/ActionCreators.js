import Axios from '../utility/axiosInstance'
import { Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import base64 from 'react-native-base64'

import * as Types from './Types'
import { navigate } from '../utility/NavigationService'
import * as routes from '../utility/routes'
import { Toast } from 'native-base'
import { showToast } from '../utility/commonMethods'

const storeToken = async (token) => {
  try {
    if (token) await AsyncStorage.setItem('userToken', token)
  } catch (e) {
    // saving error
  }
}

export const login = (dispatch, req) => {
  dispatch({ type: Types.REQUEST })
  Axios.post('/user/login', { username: req.user, password: req.pass })
    .then((res) => {
      if (res?.data?.error) {
        Alert.alert('', res?.data?.error ?? 'Login Failed')
        return
      }
      let token
      const { user } = (res && res.data) || null
      const { apitoken, uid, mail, name } = user

      if (apitoken.token) {
        token = base64.encode(`${apitoken.token}:`)
      }

      storeToken(token)
      dispatch({ type: Types.LOGIN, token, uid, mail, name })
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[LOGIN HANDLE]: ', err)
      Alert.alert('Authentication Failed', 'please check username or password')
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const sendOtp = async (dispatch, req) => {
  Axios.post('/send/otp', req)
    .then(() => {})
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('[sendOtp HANDLE]: ', err)
      Alert.alert('', 'Failed to send OTP try Again')
    })
}

export const verifyOtp = (dispatch, req) => {
  dispatch({ type: Types.REQUEST })
  let flag = true
  const { signUpData, otp } = req
  Axios.post('/verify/otp', { mobile: signUpData.mobile_number, otp })
    .then((res) => {
      if (res?.data?.error) {
        Alert.alert('', res?.data?.error ?? 'Otp Verfication Failed')
        return
      }
      if (res.data && res.data.success) {
        flag = false
        Alert.alert('', 'Verified Your Account Successfully.')
        register(dispatch, signUpData)
      }
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log(err.response.data, err.response.status)
      console.log('[sendOtp HANDLE]: ', err)
    })
    .finally(() => {
      if (flag) dispatch({ type: Types.LOADER_OFF })
    })
}

export const register = (dispatch, req) => {
  Axios.post('/user/register', req)
    .then((res) => {
      if (res?.data?.error) {
        Alert.alert('', res?.data?.error ?? 'Registration failed')
        return
      }
      let token
      const { user } = (res && res.data) || null
      const { apitoken, uid, mail, name } = user

      if (apitoken.token) {
        token = base64.encode(`${apitoken.token}:`)
      }

      storeToken(token)
      dispatch({ type: Types.LOGIN, token, uid, mail, name })
    })
    .catch((err) => {
      //  eslint-disable-next-line no-console
      console.log('[REGISTER HANDLE]: ', err)
      navigate(routes.Landing)
      Alert.alert('Registration Failed', 'please contact admin')
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const logout = (dispatch) => {
  try {
    AsyncStorage.clear()
    dispatch({ type: Types.LOGOUT })
  } catch (error) {
    //eslint-disable-next-line no-console
    console.log('[LOGOUT HANDLE]: ', error)
  }
}

export const retrieveToken = async (dispatch) => {
  try {
    const userToken = await AsyncStorage.getItem('userToken')
    if (userToken) {
      dispatch({ type: Types.RETRIEVE_TOKEN, userToken })
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('[retrieveToken HANDLE]: ', error)
  }
}

export const getProfile = (dispatch, data, loaderOff = false) => {
  //for showing noLoader in case of background fetch
  if (!loaderOff) {
    dispatch({ type: Types.REQUEST })
  }
  Axios.get('/student_profile')
    .then((res) => {
      if (res.status === 200 && res.data && res.data.length) {
        const [
          { name, uid, profile_image, pid, exam_category, mobile_number }
        ] = res.data
        dispatch({
          type: Types.GET_PROFILE,
          name,
          uid,
          profile_image,
          pid,
          exam_category,
          mobile_number
        })
      }
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('[getProfile HANDLE]: ', err)
      Toast.show({
        text: 'Something went wrong! try signing again',
        type: 'danger',
        duration: 1500
      })
      logout(dispatch)
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}

export const updateProfile = () => {
  // try {
  //   Axios.get('/post').then(() => {
  //     dispatch(Types.SET_PROFILE)
  //   })
  // } catch (err) {
  //   // eslint-disable-next-line no-console
  //   console.log('[getUpdate HANDLE]: ', err)
  // }
}

export const forgetPassword = (dispatch, req) => {
  dispatch({ type: Types.REQUEST })
  Axios.post('/forgot/password', req)
    .then((res) => {
      if (res.status === 200) {
        flag = false
        Toast.show({
          text: 'Password reset successfully',
          type: 'success'
        })
      }
      navigate(routes.SignIn)
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('[sendOtp HANDLE]: ', err)
      showToast('Something went wrong', 'danger')
    })
    .finally(() => {
      dispatch({ type: Types.LOADER_OFF })
    })
}
