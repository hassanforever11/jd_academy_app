import * as Types from './Types'

export default function (state, action) {
  switch (action.type) {
    case Types.REQUEST:
      return { ...state, userLoading: true }
    case Types.LOADER_OFF:
      return { ...state, userLoading: false }
    case Types.GET_CATEGORY:
      return {
        ...state,
        category: action.category
      }
    case Types.GET_COURSE_LISTING:
      return {
        ...state,
        tabData: { ...state.tabData, [action.tid]: action.courseList }
      }
    case Types.GET_SESSION_LISTING:
      return {
        ...state,
        sessionList: action.sessionList,
        videoList: null,
        notes: null,
        quiz: null
      }
    case Types.GET_VIDEOS:
      return { ...state, videoList: action.videoList }
    case Types.GET_NOTES:
      return { ...state, notes: action.notes }
    case Types.GET_QUIZ:
      return { ...state, quiz: action.quiz }
    case Types.REQUEST_QUIZ_DATA:
      return { ...state, userLoading: true, quizData: null }
    case Types.GET_QUIZ_DATA:
      return { ...state, quizData: action.quizData }
    case Types.ADD_TO_LIBRARY:
      return { ...state, library: action.library }
    case Types.ABOUT_US:
      return { ...state, aboutUs: action.aboutUs }
    default:
      return state
  }
}
