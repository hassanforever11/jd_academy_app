import React, { useContext } from 'react'
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Left,
  Right,
  Body,
  Icon,
  Title,
  Button,
  H3
} from 'native-base'
import { StyleSheet, SafeAreaView, StatusBar } from 'react-native'
import { UserContext } from '../context/Contexts'
import colors from '../styles/colors'
import * as routes from '../utility/routes'
import NoDataFound from './NoDataFound'
import Ionicons from 'react-native-vector-icons/Ionicons'

const ListComponent = ({
  titleKey,
  route,
  navigation,
  data,
  handleClick,
  rightComponent,
  tryAgain
}) => {
  const { userLoading } = useContext(UserContext)

  if (userLoading) return null

  return (
    <Container>
      <Content>
        {data && data.length ? (
          <List style={styles.list}>
            {data.map((item, i) => (
              <ListItem key={i} onPress={() => handleClick(item)}>
                <Left>
                  <Text>
                    {`${i + 1}. `}
                    {item[titleKey] || ``}
                  </Text>
                </Left>
                {rightComponent || (
                  <Right>
                    <Ionicons name='arrow-forward' size={16} />
                  </Right>
                )}
              </ListItem>
            ))}
          </List>
        ) : (
          <NoDataFound tryAgain={tryAgain} />
        )}
      </Content>
    </Container>
  )
}

export default ListComponent

const styles = StyleSheet.create({
  list: {
    marginBottom: 150
  }
})
