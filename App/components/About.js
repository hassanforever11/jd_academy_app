import React, { useContext, useEffect } from 'react'
import { View, StyleSheet } from 'react-native'
import { UserContext } from '../context/Contexts'
import { Container, Content, Text } from 'native-base'
import NoDataFound from './NoDataFound'
import { Paragraph } from 'react-native-paper'
import colors from '../styles/colors'

const About = () => {
  const { userActions, aboutUs } = useContext(UserContext)
  const { description, title } = aboutUs || {}

  useEffect(() => {
    if (!aboutUs) userActions.fetchAboutUs()
  }, [aboutUs])

  return (
    <Container>
      <Content padder>
        {description ? (
          <View>
            <Paragraph>
              <Text style={styles.descriptionTitle}>
                {title}
                {'\n'}
              </Text>
              {description}
            </Paragraph>
          </View>
        ) : (
          <NoDataFound />
        )}
      </Content>
    </Container>
  )
}

export default About

const styles = StyleSheet.create({
  descriptionTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.lightBlack,
    marginBottom: 10
  }
})
