import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Dimensions } from 'react-native'
import Pdf from 'react-native-pdf'
import Orientation from 'react-native-orientation'

export default function Notes({ route }) {
  const [error, seterror] = useState(false)

  const {
    params: {
      data: { title, notes_file, description }
    }
  } = route
  const source = { uri: notes_file }

  useEffect(() => {
    Orientation.unlockAllOrientations()
    return () => {
      Orientation.lockToPortrait()
    }
  }, [])

  return (
    <View style={styles.container}>
      {error ? (
        <Text>{description}</Text>
      ) : (
        <Pdf
          source={source}
          onError={(error) => {
            seterror(true)
          }}
          style={styles.pdf}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginTop: 25
  },
  pdf: {
    flex: 1,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
})
