import React, { useRef, useState, useCallback, useEffect } from 'react'
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableNativeFeedback,
  ActivityIndicator,
  BackHandler,
  StatusBar
} from 'react-native'
import Video from 'react-native-video'
import Slider from '@react-native-community/slider'
import Icon from 'react-native-vector-icons/FontAwesome5'
import Orientation from 'react-native-orientation'
import colors from '../styles/colors'
import { Container, Content, Title, Text, H3 } from 'native-base'
import { Paragraph } from 'react-native-paper'
import { useFocusEffect } from '@react-navigation/native'
import { getTime } from '../utility/commonMethods'

const { width } = Dimensions.get('window')

const VideoPlayer = ({ route }) => {
  let mount = null
  const {
    video_link: link,
    video_title: title,
    video_description: description
  } = route.params.data
  // video player ref
  const player = useRef(null)

  useEffect(() => {
    mount = true
    return () => {
      mount = null
    }
  }, [])

  // state for all atttributes of player
  const [currentTime, setcurrentTime] = useState(0)
  const [duration, setduration] = useState(0.1)
  const [paused, setpaused] = useState(false)
  const [overlay, setoverlay] = useState(true)
  const [fullscreen, setfullscreen] = useState(false)
  const [isBuffering, setisBuffering] = useState(true)

  // focus effect ties with navigation lifecycle
  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (fullscreen) {
          toggleFullscreen()
          return true
        } else {
          return false
        }
      }

      BackHandler.addEventListener('hardwareBackPress', onBackPress)

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress)
    }, [fullscreen, toggleFullscreen])
  )

  const load = ({ duration }) => setduration(duration) // now here the duration is update on load video
  const progress = ({ currentTime }) => setcurrentTime(currentTime) // here the current time is upated

  const backward = () => {
    player.current.seek(currentTime - 5)
  }

  const forward = () => {
    player.current.seek(currentTime + 5) // here the video is seek to 5 sec forward
  }

  const onslide = (slide) => {
    player.current.seek(slide * duration) // here the upation is maked for video seeking
  }

  const togglePlayPause = () => {
    setpaused(!paused)
  }

  const toggleControl = () => {
    setoverlay(!overlay)
  }

  const onBuffer = (data) => {
    setisBuffering(data.isBuffering)
  }

  const toggleFullscreen = () => {
    if (fullscreen) {
      Orientation.lockToPortrait()
    } else {
      Orientation.lockToLandscape()
    }
    setfullscreen(!fullscreen)
  }

  return (
    <Container>
      {/* <StatusBar hidden={true} /> */}
      <View style={fullscreen ? styles.fullscreenVideo : styles.video}>
        <Video
          fullscreen={fullscreen}
          paused={paused} // this will manage the pause and play
          ref={player}
          source={{ uri: link }}
          style={{ ...StyleSheet.absoluteFill }}
          resizeMode='cover'
          onLoad={load}
          onProgress={progress}
          onBuffer={onBuffer}
          // onVideoEnd={onEndVideo}
        />
        <View style={styles.overlay}>
          {overlay ? (
            <View style={styles.controls}>
              <View style={[styles.overlaySet, styles.sliderCont]}>
                <Text style={styles.timerText}>{getTime(currentTime)}</Text>
                <Slider
                  // we want to add some param here
                  style={styles.slider}
                  maximumTrackTintColor={colors.white}
                  minimumTrackTintColor={colors.white}
                  thumbTintColor={colors.white} // now the slider and the time will work
                  value={currentTime / duration} // slier input is 0 - 1 only so we want to convert sec to 0 - 1
                  onValueChange={onslide}
                />
                <Text style={styles.timerText}>{getTime(duration)} </Text>
              </View>
              <View style={[styles.overlaySet]}>
                <Icon name='backward' style={styles.icon} onPress={backward} />
                <Icon
                  name={paused ? 'play' : 'pause'}
                  style={styles.icon}
                  onPress={togglePlayPause}
                />
                <Icon name='forward' style={styles.icon} onPress={forward} />
                <Icon
                  onPress={toggleFullscreen}
                  name={fullscreen ? 'compress' : 'expand'}
                  style={styles.icon}
                />
              </View>
            </View>
          ) : null}
          <View style={styles.container}>
            <TouchableNativeFeedback onPress={toggleControl}>
              <View style={[styles.container, styles.overlaySet]}>
                {isBuffering ? (
                  <ActivityIndicator size='large' color={colors.white} />
                ) : null}
              </View>
            </TouchableNativeFeedback>
          </View>
        </View>
      </View>
      {fullscreen ? null : (
        <Content padder>
          {title ? (
            <H3 style={styles.boldText}>
              {title || 'Title'}
              {'\n'}
            </H3>
          ) : null}
          {description ? (
            <Paragraph>
              <Text style={styles.boldText}>
                Video description
                {'\n'}
              </Text>

              {description}
            </Paragraph>
          ) : null}
        </Content>
      )}
    </Container>
  )
}

export default VideoPlayer

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  overlay: {
    ...StyleSheet.absoluteFillObject
  },
  overlaySet: {
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  controls: {
    zIndex: 100,
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0
  },
  icon: {
    color: 'white',
    height: 35,
    backgroundColor: 'black',
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 20
  },
  slider: {
    flex: 6,
    backgroundColor: colors.black
  },
  sliderCont: {
    backgroundColor: colors.black,
    height: 30,
    paddingHorizontal: 4
  },
  timerText: {
    color: colors.white,
    fontWeight: 'bold'
  },
  timer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 5
  },
  video: { width, height: width * 0.6, backgroundColor: 'black' },
  fullscreenVideo: {
    backgroundColor: 'black',
    ...StyleSheet.absoluteFill,
    elevation: 1
  },
  boldText: {
    fontWeight: 'bold'
  }
})
