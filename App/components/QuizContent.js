import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import {
  Text,
  Title,
  List,
  ListItem,
  Left,
  Right,
  Icon,
  Item,
  Input,
  Body
} from 'native-base'
import colors from '../styles/colors'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

export default function QuizContent({
  data,
  handleClick,
  selectedData,
  handleFlag,
  flagData,
  resultPage
}) {
  return (
    <>
      {data &&
        data.map((item, index) => {
          const count = index + 1
          const { question_title, options } = item
          const flag = flagData[count]
          return (
            <View key={count} style={styles.questionWrap}>
              <Item style={{ borderBottomWidth: 0 }}>
                <Left>
                  <Text>{`Q${count}.`}</Text>
                </Left>
                <Right>
                  {flag ? (
                    <FontAwesome
                      onPress={() => handleFlag(count)}
                      name='bookmark'
                      size={24}
                      style={{ color: colors.green01 }}
                    />
                  ) : (
                    <FontAwesome
                      size={24}
                      name='bookmark-o'
                      onPress={() => handleFlag(count)}
                    />
                  )}
                </Right>
              </Item>
              <Text>{question_title}</Text>
              <List>
                {options.map(({ option, correct, id }, i) => {
                  let isWrong, skipped
                  let selected = selectedData[count]?.id === id ?? false
                  if (resultPage) {
                    if (!selectedData[count] && correct === '1') skipped = true
                    if (selected && correct !== '1') isWrong = true
                    selected = correct === '1'
                  }
                  return (
                    <ListItem
                      style={
                        selected
                          ? styles.selected
                          : isWrong
                          ? styles.wrongAnswer
                          : {}
                      }
                      key={i}
                      onPress={() => handleClick(count, option, correct, id)}
                    >
                      <Body>
                        <Text>{`${i + 1}. ${option}`}</Text>
                        {isWrong ? <Text note>Wrong answer</Text> : null}
                        {selected && resultPage && !skipped ? (
                          <Text note>Correct answer</Text>
                        ) : null}
                        {skipped ? <Text note>Skipped</Text> : null}
                      </Body>
                      <Right>
                        {selected ? (
                          <Icon
                            name='checkmark-circle'
                            style={{ color: colors.green01 }}
                          />
                        ) : isWrong ? (
                          <FontAwesome
                            name='info-circle'
                            style={{ color: colors.red }}
                            size={20}
                          />
                        ) : null}
                      </Right>
                    </ListItem>
                  )
                })}
              </List>
            </View>
          )
        })}
    </>
  )
}

const styles = StyleSheet.create({
  questionWrap: {
    marginBottom: 16
  },
  selected: {
    borderBottomColor: colors.green01,
    borderBottomWidth: 2
  },
  wrongAnswer: {
    borderBottomColor: colors.red,
    borderBottomWidth: 2
  }
})
