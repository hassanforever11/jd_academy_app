import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBar
} from 'react-native'
import * as Animatable from 'react-native-animatable'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
const LogoUrl = require('../../../assets/images/dJ-LOGO-FINAL.png')
import * as routes from '../../utility/routes'
import colors from '../../styles/colors'

const Landing = ({ navigation }) => {
  return (
    <View style={styles.container}>
      {/* <StatusBar backgroundColor={colors.white} barStyle='dark-content' /> */}
      <View style={styles.header}>
        <Animatable.Image
          animation='bounceIn'
          duraton='1500'
          source={LogoUrl}
          style={styles.logo}
          resizeMode='stretch'
        />
      </View>
      <Animatable.View style={styles.footer} animation='fadeInUpBig'>
        <Text style={styles.title}>
          Learn Live from India&apos;s Best Educators
        </Text>
        <Text style={styles.text}>Sign in with your Account</Text>
        <View style={styles.button}>
          <TouchableOpacity onPress={() => navigation.navigate(routes.SignIn)}>
            <View style={styles.signIn}>
              <Text style={styles.textSign}>Get Started</Text>
              <MaterialIcons name='navigate-next' color='grey' size={20} />
            </View>
          </TouchableOpacity>
        </View>
      </Animatable.View>
    </View>
  )
}

const { height } = Dimensions.get('screen')
const height_logo = height * 0.45

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    flex: 1,
    backgroundColor: colors.baseColor,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30
  },
  logo: {
    width: height_logo,
    height: height_logo
  },
  title: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  },
  text: {
    color: '#fff',
    marginTop: 5
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  textSign: {
    color: 'grey',
    fontWeight: 'bold'
  }
})

export default Landing
