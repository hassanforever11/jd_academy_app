import React, { useContext } from 'react'

import OTP from '../../components/OTPScreen'
import { AuthContext } from '../../context/Contexts'

const OTPScreen = ({ route, navigation }) => {
  const { params } = route
  const { actions } = useContext(AuthContext)

  const handleOtpVerification = (otp) => {
    actions.verifyOtp({ otp, signUpData: params })
  }

  const handleResendOtp = () => {
    actions.sendOtp({ mobile: params.mobile_number })
  }

  return (
    <OTP
      verifyOtp={handleOtpVerification}
      resendOtp={handleResendOtp}
      mobile={(params && params.mobile_number) || 'your mobile number'}
      navigation={navigation}
      footerText={'Verify and Create Account'}
    />
  )
}

export default OTPScreen
