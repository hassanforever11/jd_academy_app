import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
  SafeAreaView
} from 'react-native'
import { Container, Button } from 'native-base'
import colors from '../../styles/colors'
import { navigate } from '../../utility/NavigationService'
import * as routes from '../../utility/routes'
import NoDataFound from '../../components/NoDataFound'

export default function ({ data, tryAgain, userLoading }) {
  const clickHandler = (details) => {
    navigate(routes.CourseDetails, { details })
  }

  if (userLoading) return null

  return (
    <Container>
      <SafeAreaView style={styles.safeArea}>
        {data && data.length ? (
          <FlatList
            style={styles.list}
            contentContainerStyle={styles.listContainer}
            data={data}
            horizontal={false}
            numColumns={2}
            keyExtractor={(item, i) => i}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.card}
                onPress={() => clickHandler(item)}
              >
                {/* <View style={styles.cardHeader}>
                    <Image
                      style={styles.icon}
                      source={{
                        uri:
                          'https://img.icons8.com/flat_round/64/000000/hearts.png'
                      }}
                    />
                  </View> */}
                <View style={styles.cardContent}>
                  <Image
                    style={styles.courseImage}
                    source={{ uri: item.course_image }}
                  />
                </View>
                <View style={styles.cardFooter}>
                  <View
                  // style={{ alignItems: 'center', justifyContent: 'center' }}
                  >
                    <Text style={styles.title}>{item.course_title}</Text>
                    <View style={styles.priceContainer}>
                      {item.is_free === '1' ? (
                        <Text style={styles.price}>Free</Text>
                      ) : item.course_offer_price !== '0' ? (
                        <>
                          <Text style={styles.price}>
                            &#x20B9;{item.course_offer_price}&nbsp;
                          </Text>
                          <Text style={styles.strikeThrough}>
                            &#x20B9;{item.course_price}
                          </Text>
                        </>
                      ) : (
                        <Text style={styles.price}>
                          &#x20B9;{item.course_price}&nbsp;
                        </Text>
                      )}
                    </View>
                    {/* <Text style={styles.position}>{item.position}</Text> */}
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        ) : (
          <NoDataFound tryAgain={tryAgain} />
        )}
      </SafeAreaView>
    </Container>
  )
}

const deviceWidth = Dimensions.get('window').width

const styles = StyleSheet.create({
  container: {
    // flex: 1
    // marginTop: 20
  },
  list: {
    paddingHorizontal: 5
    // backgroundColor: '#000'
  },
  listContainer: {
    // alignItems: 'center'
  },
  /******** card **************/
  card: {
    shadowColor: colors.gray02,
    shadowOffset: {
      width: 0,
      height: 6
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    marginVertical: 5,
    backgroundColor: 'white',
    flexBasis: '47.5%',
    marginHorizontal: 5
  },
  cardFooter: {
    paddingVertical: 8,
    paddingHorizontal: 4,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row'
    // alignItems: 'center',
    // justifyContent: 'center'
  },
  cardContent: {
    // paddingVertical: 12.5,
    paddingHorizontal: 16
  },
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1
  },
  courseImage: {
    width: deviceWidth / 2 - 16,
    height: deviceWidth / 2 - 16,
    // borderRadius: 60,
    alignSelf: 'center',
    borderColor: '#fff',
    borderWidth: 1
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 4,
    color: colors.lightBlack
  },
  price: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.baseColor
  },
  safeArea: { flex: 1, marginBottom: 170 },
  priceContainer: {
    flexDirection: 'row'
  },
  strikeThrough: {
    fontSize: 12,
    textDecorationLine: 'line-through',
    textAlignVertical: 'bottom'
  }
})
