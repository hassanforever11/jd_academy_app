import React, { useContext, useEffect } from 'react'
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Left,
  Right,
  Body,
  Icon,
  Title,
  Button,
  H3
} from 'native-base'
import { UserContext } from '../../context/Contexts'
import colors from '../../styles/colors'
import * as routes from '../../utility/routes'
import ListComponent from '../../components/ListComponent'

const SessionList = ({ route, navigation }) => {
  const { userActions, sessionList } = useContext(UserContext)
  const { params } = route

  useEffect(() => {
    userActions.getSessionList(params.cid)
  }, [])

  const handleClick = (data) => {
    navigation.navigate(routes.CourseMaterial, { sid: data.sid })
  }

  const tryAgainHandle = () => {
    userActions.getSessionList(params.cid)
  }

  return (
    <ListComponent
      titleKey='session_title'
      data={sessionList}
      handleClick={handleClick}
      tryAgain={tryAgainHandle}
    />
  )
}

export default SessionList
