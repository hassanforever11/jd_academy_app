import React, { useEffect, useContext, useState } from 'react'
import {
  Container,
  Content,
  Tab,
  Tabs,
  TabHeading,
  Text,
  ScrollableTab,
  Left,
  Right,
  List,
  ListItem,
  Icon
} from 'native-base'
import { AuthContext, UserContext } from '../../context/Contexts'
import CourseList from './CourseList'
import * as routes from '../../utility/routes'
import colors from '../../styles/colors'
import { StyleSheet } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import Octicons from 'react-native-vector-icons/Octicons'
import Ionicons from 'react-native-vector-icons/Ionicons'

const Shop = ({ navigation }) => {
  const { actions, exam_category, pid } = useContext(AuthContext)
  const { userActions, tabData, userLoading } = useContext(UserContext)

  useEffect(() => {
    if (!pid) {
      actions.getProfile()
    } else if (!exam_category) {
      navigation.navigate(routes.Exam)
    } else if (exam_category && exam_category.length) {
      handleTabChange({ name: exam_category[0].name })
    }
  }, [exam_category, pid])

  const handleTabChange = (data, tryAgain = false) => {
    if (data && data.name) {
      if (tabData[data.name] && !tryAgain) {
        return
      }
    } else if (tabData[data.ref.props.heading] && !tryAgain) {
      return
    }
    userActions.getCourseList(exam_category[data.i || 0].name)
  }

  const tryAgainHandle = () => {
    handleTabChange({ i: curTabIndex }, true)
  }

  const navigateToMyLibrary = () => {
    navigation.navigate(routes.MyLibrary)
  }

  const navigateToLiveSessions = () => {
    navigation.navigate(routes.LiveSessions)
  }

  return (
    <Container>
      <Content scrollEnabled={false}>
        <List>
          <ListItem onPress={navigateToMyLibrary}>
            <Left>
              <MaterialIcon name='library-books' size={20} />
              <Text>&nbsp;&nbsp;My Library</Text>
            </Left>
            <Right>
              <Ionicons name='arrow-forward' size={16} />
            </Right>
          </ListItem>
          <ListItem onPress={navigateToLiveSessions}>
            <Left>
              <Octicons name='broadcast' size={20} />
              <Text>&nbsp;&nbsp;Live Sessions</Text>
            </Left>
            <Right>
              <Ionicons name='arrow-forward' size={16} />
            </Right>
          </ListItem>
        </List>
        {exam_category && exam_category.length ? (
          <Tabs
            tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
            // tabBarActiveTextColor={}
            onChangeTab={handleTabChange}
            renderTabBar={() => (
              <ScrollableTab tabsContainerStyle={styles.tabsContainerStyle} />
            )}
          >
            {exam_category.map((exam, index) => (
              <Tab
                activeTabStyle={styles.activeTabStyle}
                activeTextStyle={styles.activeTextStyle}
                tabStyle={styles.tabStyle}
                textStyle={styles.textStyle}
                key={index}
                heading={exam.name}
              >
                <CourseList
                  tryAgain={tryAgainHandle}
                  data={tabData[exam.name]}
                  userLoading={userLoading}
                />
              </Tab>
            ))}
          </Tabs>
        ) : null}
      </Content>
    </Container>
  )
}

export default Shop

const styles = StyleSheet.create({
  tabBarUnderlineStyle: { backgroundColor: colors.baseColor },
  tabsContainerStyle: {
    backgroundColor: colors.white,
    justifyContent: 'flex-start'
  },
  activeTabStyle: {
    backgroundColor: colors.white,
    minWidth: 100
  },
  activeTextStyle: {
    color: colors.baseColor,
    fontWeight: 'bold'
  },
  tabStyle: { backgroundColor: colors.white, minWidth: 100 },
  textStyle: { color: colors.lightBlack }
})
