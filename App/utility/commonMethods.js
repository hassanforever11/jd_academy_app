import { Toast } from 'native-base'

export const showToast = (text, type, duration) => {
  Toast.show({ text, type, duration: duration || 1000, buttonText: 'Okay' })
}

export const getTime = (t) => {
  const digit = (n) => (n < 10 ? `0${n}` : `${n}`)
  // const t = Math.round(time);
  const sec = digit(Math.floor(t % 60))
  const min = digit(Math.floor((t / 60) % 60))
  const hr = digit(Math.floor((t / 3600) % 60))
  return hr + ':' + min + ':' + sec // this will convert sec to timer string
  // 33 -> 00:00:33
  // this is done here
}
