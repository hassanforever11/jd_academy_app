import React, { useContext } from 'react'
import { StyleSheet, StatusBar } from 'react-native'
import { Root } from 'native-base'

import AppNavigation from './AppNavigator'
import Loader from '../components/Loader'
import { AuthContext, UserContext } from '../context/Contexts'

const NetworkInterceptor = () => {
  const { isLoading } = useContext(AuthContext)
  const { userLoading } = useContext(UserContext)

  return (
    <Root>
      <StatusBar hidden={true} />
      <Loader visible={isLoading || userLoading} />
      <AppNavigation />
    </Root>
  )
}

export default NetworkInterceptor

const styles = StyleSheet.create({})
