import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'

import Shop from '../containers/Shop'
import Notification from '../containers/Notification'
import CourseDetailsScreen from '../containers/Shop/CourseDetails'
import About from '../components/About'
import colors from '../styles/colors'
import * as routes from '../utility/routes'
import SessionList from '../containers/Shop/SessionList'
import CourseDetail from '../containers/Shop/CourseDetails'
import CourseMaterial from '../containers/Shop/CourseMaterial'
import MyLibrary from '../containers/MyLibrary'
import LiveSessions from '../containers/LiveSessions'
import VideoPlayer from '../components/VideoPlayer'
import Notes from '../components/Notes'
import Quiz from '../components/Quiz'

const ShopStack = createStackNavigator()

const ShopStackScreen = ({ navigation }) => (
  <ShopStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: colors.baseColor
      },
      headerTintColor: colors.white,
      headerTitleStyle: {
        fontWeight: 'bold'
      }
    }}
  >
    <ShopStack.Screen
      name={routes.Shop}
      component={MainTabScreen}
      options={{
        title: 'JD Academy',
        headerLeft: () => (
          <Icon.Button
            name='md-menu'
            size={30}
            backgroundColor={colors.baseColor}
            onPress={() => navigation.openDrawer()}
          />
        )
      }}
    />
    <ShopStack.Screen
      options={{ title: 'Sessions' }}
      name={routes.SessionList}
      component={SessionList}
    />
    <ShopStack.Screen
      options={{ title: 'Course Details' }}
      name={routes.CourseDetails}
      component={CourseDetail}
    />
    <ShopStack.Screen
      options={{ title: 'Course Material' }}
      name={routes.CourseMaterial}
      component={CourseMaterial}
    />
    <ShopStack.Screen
      options={{ title: 'My Library' }}
      name={routes.MyLibrary}
      component={MyLibrary}
    />
    <ShopStack.Screen
      options={{ title: 'Live Sessions' }}
      name={routes.LiveSessions}
      component={LiveSessions}
    />
    <ShopStack.Screen
      options={{ headerShown: false }}
      name={routes.VideoPlayer}
      component={VideoPlayer}
    />
    <ShopStack.Screen name={routes.Notes} component={Notes} />
    <ShopStack.Screen
      options={{ headerShown: false }}
      name={routes.Quiz}
      component={Quiz}
    />
  </ShopStack.Navigator>
)
const Tab = createBottomTabNavigator()

function MainTabScreen() {
  return (
    <Tab.Navigator
      initialRouteName={routes.Shop}
      activeColor={colors.white}
      tabBarOptions={{
        activeTintColor: colors.baseColor,
        inactiveTintColor: colors.lightBlack
      }}
    >
      <Tab.Screen
        name={routes.Shop}
        component={Shop}
        options={{
          tabBarColor: colors.baseColor,
          tabBarIcon: ({ color, size }) => (
            <Icon name='md-book' color={color} size={size} />
          )
        }}
      />
      <Tab.Screen
        name={routes.Notification}
        component={Notification}
        options={{
          tabBarColor: colors.baseBlue,
          tabBarIcon: ({ color, size }) => (
            <Icon name='md-notifications' color={color} size={size} />
          )
        }}
      />
      <Tab.Screen
        name={routes.About}
        component={About}
        options={{
          tabBarColor: colors.black,
          tabBarIcon: ({ color, size }) => (
            <Icon name='md-person' color={color} size={size} />
          )
        }}
      />
    </Tab.Navigator>
  )
}

export default ShopStackScreen
