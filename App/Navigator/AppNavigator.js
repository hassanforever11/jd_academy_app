import React, { useContext, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { navigationRef } from '../utility/NavigationService'
import AuthStackScreen from './AuthStackScreen'
import MainTabScreen from './MainTabScreens'
import DrawerContent from './DrawerContent'
import SupportScreen from '../components/Support'
import ExamScreen from '../components/Exam'
import ExamDetailsScreen from '../components/ExamDetails'
import { AuthContext } from '../context/Contexts'
import * as routes from '../utility/routes'

const Drawer = createDrawerNavigator()

const Navigator = () => {
  const { userToken, actions } = useContext(AuthContext)

  useEffect(() => {
    actions.retrieveToken()
  }, [])

  return (
    <NavigationContainer ref={navigationRef}>
      {userToken ? (
        <Drawer.Navigator
          initialRouteName={routes.Home}
          drawerContent={(props) => <DrawerContent {...props} />}
        >
          <Drawer.Screen
            name={routes.Home}
            options={{ unmountOnBlur: true }}
            component={MainTabScreen}
          />
          <Drawer.Screen name={routes.Exam} component={ExamScreen} />
          <Drawer.Screen
            options={{ unmountOnBlur: true }}
            name={routes.ExamDetails}
            component={ExamDetailsScreen}
          />
          <Drawer.Screen name={routes.Support} component={SupportScreen} />
        </Drawer.Navigator>
      ) : (
        <AuthStackScreen />
      )}
    </NavigationContainer>
  )
}

export default Navigator
