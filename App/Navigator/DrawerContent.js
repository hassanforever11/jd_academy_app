import React, { useContext } from 'react'
import { View } from 'react-native'
import {
  // useTheme,
  Avatar,
  Title,
  Caption,
  // Paragraph,
  Drawer
  // Text,
  // TouchableRipple
  // Switch
} from 'react-native-paper'

import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer'
import Icon from 'react-native-vector-icons/Ionicons'
import styles from '../styles'
import * as routes from '../utility/routes'

import { AuthContext } from '../context/Contexts'

export default function (props) {
  const { actions, profile_image, name, mobile_number } = useContext(
    AuthContext
  )

  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <View style={{ flexDirection: 'row', marginTop: 15 }}>
              <Avatar.Image
                source={{
                  uri: profile_image
                    ? profile_image
                    : 'https://api.adorable.io/avatars/50/abott@adorable.png'
                }}
                size={50}
              />
              <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                <Title style={styles.title}>{name}</Title>
                <Caption style={styles.caption}>{mobile_number}</Caption>
              </View>
            </View>

            {/* <View style={styles.row}>
              <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>
                  80
                </Paragraph>
                <Caption style={styles.caption}>Following</Caption>
              </View>
              <View style={styles.section}>
                <Paragraph style={[styles.paragraph, styles.caption]}>
                  100
                </Paragraph>
                <Caption style={styles.caption}>Followers</Caption>
              </View>
            </View> */}
          </View>

          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name='document-text-sharp' color={color} size={size} />
              )}
              label='Exams'
              onPress={() => {
                props.navigation.navigate(routes.Exam)
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name='md-book' color={color} size={size} />
              )}
              label='Shop'
              onPress={() => {
                props.navigation.navigate(routes.Shop)
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name='md-notifications' color={color} size={size} />
              )}
              label='Notification'
              onPress={() => {
                props.navigation.navigate(routes.Notification)
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name='md-person' color={color} size={size} />
              )}
              label='About'
              onPress={() => {
                props.navigation.navigate(routes.About)
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name='md-settings' color={color} size={size} />
              )}
              label='Settings'
              // onPress={() => {
              //   props.navigation.navigate(routes.Settings)
              // }}
            />
            {/* <DrawerItem
              icon={({ color, size }) => (
                <Icon name='contacts' color={color} size={size} />
              )}
              label='Support'
              onPress={() => {
                props.navigation.navigate(routes.Support)
              }}
            /> */}
          </Drawer.Section>
          {/* <Drawer.Section title='Preferences'>
            <TouchableRipple onPress={() => {}}>
              <View style={styles.preference}>
                <Text>Dark Theme</Text>
                <View pointerEvents='none'>
                  <Switch value={paperTheme.dark} />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section> */}
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({ color, size }) => (
            <Icon name='md-log-out' color={color} size={size} />
          )}
          label='Sign Out'
          onPress={() => actions.signOut()}
        />
      </Drawer.Section>
    </View>
  )
}
