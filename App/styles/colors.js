export default {
  black: '#000',
  lightBlack: '#696969',
  white: '#fff',
  green01: '#008388',
  green02: '#02656b',
  darkOrange: '#d93900',
  lightGray: '#d8d8d8',
  red: '#CA0B00',
  pink: '#fc4c54',
  gray01: '#f3f3f3',
  gray02: '#00000021',
  brown01: '#ad8763',
  brown02: '#7d4918',
  blue: '#4995cd',
  baseColor: '#9a1b24',
  baseBlue: '#00a8e7',
  darkBlue: '#05375a'
}
