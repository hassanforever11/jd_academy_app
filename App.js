/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react'
import SplashScreen from 'react-native-splash-screen'
import 'react-native-gesture-handler'
import GlobalState from './App/context/GlobalState'
import UserState from './App/context/UserContext/UserStates'
import RootApp from './App/Navigator'

//For Now to ignore warning by native base will be fxed in future update not released yet
import { YellowBox } from 'react-native'

YellowBox.ignoreWarnings([
  'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`'
])
const App = () => {
  useEffect(() => {
    SplashScreen.hide()
  }, [])

  return (
    <GlobalState>
      <UserState>
        <RootApp />
      </UserState>
    </GlobalState>
  )
}

export default App
